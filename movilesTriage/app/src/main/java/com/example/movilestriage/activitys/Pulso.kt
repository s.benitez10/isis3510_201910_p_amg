package com.example.movilestriage.activitys

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import com.example.movilestriage.R
import com.google.android.gms.wearable.*

import kotlinx.android.synthetic.main.activity_pulso.*

class Pulso : AppCompatActivity(),DataClient.OnDataChangedListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pulso)

    }
    override fun onResume() {
        super.onResume()
        Wearable.getDataClient(this).addListener(this)
        startService(Intent(this,Escuchador::class.java))
    }


    override fun onDataChanged(dataEvents: DataEventBuffer) {
        dataEvents.forEach { event ->


                event.dataItem.also { item ->
                    if (item.uri.path.compareTo("/pulso/") == 0) {
                        DataMapItem.fromDataItem(item).dataMap.apply {
                            val valor= getInt("MY_KEY")
                            Log.d("Valor","valor " + valor)
                        }
                    }
                }
            }
        }

}