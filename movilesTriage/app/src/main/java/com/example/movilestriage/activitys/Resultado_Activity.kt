package com.example.movilestriage.activitys

import android.content.Intent
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.widget.Button
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import com.example.movilestriage.R
import com.example.movilestriage.common.common
import com.example.movilestriage.database.FirebaseManager
import com.example.movilestriage.model.Triage
import com.example.movilestriage.model.Usuarios
import com.google.common.primitives.UnsignedBytes.toInt

import kotlinx.android.synthetic.main.activity_resultado_.*

class Resultado_Activity : AppCompatActivity() {
    val db: FirebaseManager = FirebaseManager()
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resultado_)
        usuariosTriage().execute(volver_btn)
        volver_btn.isEnabled = false
        if (common.puntuacionTriage / 5 <= 20) {
            numero_Triage.text = "5"
            numero_Triage.setTextColor(Color.BLUE)
            descripcion.text =
                " Tu condicion Actual no presenta ningun peligro para el riesgo de tu vida. Es muy probable " +
                        "que te toque esperar mas de 5 horas en urgencias o seguramente te manden a cita externa"
        } else if (common.puntuacionTriage / 5 <= 30 && common.puntuacionTriage > 20) {
            numero_Triage.text = "4"
            numero_Triage.setTextColor(Color.GREEN)
            descripcion.text =
                " Tu condicion Actual no presenta ningun peligro para el riesgo de tu vida pero la situación puede empeorar. Es muy probable " +
                        "que te toque esperar entre 2 horas y 4 horas en urgencias"
        } else if (common.puntuacionTriage / 5 <= 40 && common.puntuacionTriage > 30) {
            numero_Triage.text = "3"
            numero_Triage.setTextColor(Color.YELLOW)
            descripcion.text = " Tu condicion Actual puede presentar riesgoz amenazantes a tu vida. Es muy probable " +
                    "que te toque esperar maximo 2 horas en urgencias para que te atiendan "
        }


        volver_btn.setOnClickListener {

            agregarTriage(numero_Triage.text.toString())


        }

    }

    fun agregarTriage(valoracion:String?)
    {

        val usuario = common.nombreUsuarioActual
        Log.d("Usuario nombre",""+ usuario)
        val triage = Triage(usuario, common.edadUsuarioActual.toInt(),common.categoriaEvaluada,valoracion)
        com.example.movilestriage.activitys.db.agregarTriage(triage)

        val intent = Intent(this,MenuActivity::class.java)
        startActivity(intent)
    }

}
private class usuariosTriage : AsyncTask<Button, Void, Boolean>() {

    lateinit var butoon :Button
    override fun onPreExecute() {
        super.onPreExecute()
        db.obtenerUsuario()
    }

    override fun doInBackground(vararg params: Button?): Boolean {
        if (android.os.Debug.isDebuggerConnected())
            android.os.Debug.waitForDebugger();
        var boo = false
        butoon = params[0]!!
        while(common.nombreUsuarioActual == "" ){
         boo= true
        }
       return boo

    }

    override fun onPostExecute(result: Boolean?) {
        super.onPostExecute(result)
        if(result!!)
        {
           butoon.isEnabled = true
        }
    }


}