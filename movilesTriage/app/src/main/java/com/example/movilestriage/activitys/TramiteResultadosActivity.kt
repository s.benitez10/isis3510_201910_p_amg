package com.example.movilestriage.activitys

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.movilestriage.R
import com.example.movilestriage.database.FirebaseManager
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_main2.*
import java.security.Permission
import java.util.jar.Manifest

class TramiteResultadosActivity : AppCompatActivity() {

     private lateinit var storage: FirebaseStorage
     private lateinit var  base: FirebaseManager
     private lateinit var pdfUrl :Uri

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        title = "Resultados"
        storage= FirebaseStorage.getInstance()
        base = FirebaseManager()


        buscarArchivo.setOnClickListener {
            if(ContextCompat.checkSelfPermission(this,android.Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED)
            {
                seleccionarPdf()
            }
            else{

                ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),1)

            }
        }
        subirArchivo.setOnClickListener {
            if(pdfUrl != null) {
                subirArchivo()
            } else{
                Toast.makeText(this,"No hay archivo cargado",Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if(requestCode == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            seleccionarPdf()
        }
        else
        {
            Toast.makeText(this,"Porfavor de los permisos de revisar sus archivos para que habilitar la funcionalidad",Toast.LENGTH_LONG).show()
        }
    }


    private fun seleccionarPdf()
    {
        val getIntent =  Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("application/*");
        startActivityForResult(Intent.createChooser(getIntent,"Selecione imagen"),10)

    }
    private fun subirArchivo()
    {
        progresoArchivo.visibility = View.VISIBLE
        val storageReference = storage.getReference()
        storageReference.child("User").child("me").putFile(pdfUrl)
            .addOnSuccessListener {taskSnapshot ->
                val url = taskSnapshot.metadata!!.name
                Log.d("FirebaseManager", "Upload Successful " + pdfUrl)


            }.addOnFailureListener{
                Toast.makeText(this,"El archivo no se pudo subir ",Toast.LENGTH_LONG).show()
            }.addOnProgressListener {taskSnapshot ->


                val porcentage = ((100* taskSnapshot.bytesTransferred)/taskSnapshot.totalByteCount).toInt()
                progresoArchivo.progress = porcentage


                if (progresoArchivo.progress == 100)
                {
                    progresoArchivo.visibility = View.GONE
                    Toast.makeText(this,"El archivo subio exitosamente",Toast.LENGTH_LONG).show()
                }
            }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 10 && resultCode== Activity.RESULT_OK && data != null )
        {
            pdfUrl = data.data
            documentoText.text = data.data.path
            imagenArchivo.setImageResource(R.mipmap.recibibo_archivos_round)
        }
        else{
            Toast.makeText(this,"No hay archivo cargado",Toast.LENGTH_LONG).show()
        }
    }
}
