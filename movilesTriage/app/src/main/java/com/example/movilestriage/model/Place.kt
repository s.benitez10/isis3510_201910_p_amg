package com.example.movilestriage.model

import com.example.movilestriage.activitys.ubicacion
import com.google.firebase.firestore.GeoPoint

data class Place (

    var nombre: String?,
    var Ubicacion: GeoPoint?,
    var abierto: Boolean?,
    var calificacion: Double?,
    var espacio: Int?

){
    constructor() : this(null,null,null,null ,null)

    fun hashMapPlace() : Map<String,Any?>
    {
        return mapOf(
            "nombre" to  nombre,
            "ubicacion" to ubicacion,
            "abierto" to abierto,
            "calificacion" to calificacion,
            "espacio" to espacio

        )
    }



}