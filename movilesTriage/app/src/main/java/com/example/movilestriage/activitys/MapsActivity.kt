package com.example.movilestriage.activitys

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.ColorSpace
import android.location.Location
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.movilestriage.R
import com.example.movilestriage.common.miradorInternet
import com.example.movilestriage.database.FirebaseManager
import com.example.movilestriage.model.Place
import com.google.android.gms.location.FusedLocationProviderClient

import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.GeoDataClient
import com.google.android.gms.location.places.PlaceDetectionClient
import com.google.android.gms.location.places.Places

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.firestore.GeoPoint
import kotlinx.android.synthetic.main.no_internet_layout.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.Exception
import java.lang.StringBuilder
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import javax.net.ssl.HttpsURLConnection

lateinit var mMap: GoogleMap
class MapsActivity : AppCompatActivity(), OnMapReadyCallback {


    var mLocationPermissionGranted: Boolean = false
    private lateinit var mGeoClient: GeoDataClient
    private lateinit var mPlaceDectec: PlaceDetectionClient
    private lateinit var mFLPC: FusedLocationProviderClient
    lateinit var mUltimaUbi: Location
    lateinit var  connector: miradorInternet

    companion object {
        private const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        connector= miradorInternet(this)
        if(connector.checkeo())
        {
            setContentView(R.layout.activity_maps2)
            title ="Mapa Hospitales"
        }else
        {
            setContentView(R.layout.no_internet_layout)
            title="Sin Conexion"
            return
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        mGeoClient = Places.getGeoDataClient(this, null)
        mPlaceDectec = Places.getPlaceDetectionClient(this, null)
        mFLPC = LocationServices.getFusedLocationProviderClient(this)

        val mapFragment = supportFragmentManager
            .findFragmentById(
                com.example.movilestriage.R
                    .id.map
            ) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun getLocationPermission() {

        if (ContextCompat.checkSelfPermission(
                this.applicationContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            mLocationPermissionGranted = true
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.uiSettings.isMyLocationButtonEnabled = true

        getLocationPermission()
        obtenerUbicacion()
        obtenerHospitalesCercanos()


    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        mLocationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] === PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true
                }
            }
        }

    }

    private fun obtenerUbicacion() {
        try {
            if (mLocationPermissionGranted) {
                val result = mFLPC.lastLocation
                result.addOnCompleteListener(OnCompleteListener { task ->

                    if (task.isSuccessful) {
                        mUltimaUbi = Location(task.result!!)

                        mMap.addMarker(
                            MarkerOptions().position(
                                LatLng(
                                    mUltimaUbi.latitude,
                                    mUltimaUbi.longitude

                                )
                            ).title("Mi Ubicacion")
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                        )
                        mMap.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                LatLng(
                                    mUltimaUbi.latitude,
                                    mUltimaUbi.longitude
                                ),
                                15.0f
                            )
                        )
                        Log.d("Latitud", "${mUltimaUbi.latitude}")
                        Log.d("Longitud", "${mUltimaUbi.longitude}")
                    } else {
                        Log.d("no permisos", "Current location is null. Using defaults.");
                        Log.e("error", "Exception: %s", task.getException())
                    }
                })
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.toString())
        }
    }

    private fun obtenerHospitalesCercanos() {
        try {
            mFLPC.lastLocation.addOnSuccessListener { location ->
                val UrlRequestplaces = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${location.latitude},${location.longitude}%20&radius=1000&types=hospital|health&key=AIzaSyAEmcult29ulZu5eiEg7_0FKUYmToOHmAk"
                places().execute(UrlRequestplaces)
            }
        }
        catch (e :SecurityException){
            Log.e("ERROR", "erro",error(e))
        }
    }
}

    private class places : AsyncTask<String, Void, String>() {
        val db = FirebaseManager()

        override fun doInBackground(vararg url: String?): String {
            if (android.os.Debug.isDebuggerConnected())
                android.os.Debug.waitForDebugger()

            var builder = StringBuffer()
            for (place in url) {
                try {
                    var request = URL(place)
                    var connect = request.openConnection() as HttpsURLConnection
                    connect.requestMethod = "GET"
                    connect.connect()
                    if (connect.responseCode == HttpURLConnection.HTTP_OK) {
                        var inputStream = connect.inputStream
                        var reader = BufferedReader(InputStreamReader(inputStream))
                        var line = reader.readLine()
                        while ((line) != null) {
                            builder.append(line + "\n")
                            line = reader.readLine()

                        }
                        if (builder.length == 0) {
                            return ""
                        }
                    }

                } catch (e: Exception) {
                    Log.e("ERROr", error(e))
                }
            }
            return builder.toString()
        }

        override fun onPreExecute() {
            super.onPreExecute()
        }


        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            if (android.os.Debug.isDebuggerConnected())
                android.os.Debug.waitForDebugger()
            try {
                var place = Place()
                val json = JSONObject(result)
                val jsonArray = json.getJSONArray("results")
                val random = Random()
                for (i in 0..(jsonArray.length() - 1)) {
                    val placeOjectJs = jsonArray.getJSONObject(i)

                    val ubi = placeOjectJs.getJSONObject("geometry").getJSONObject("location")
                    val ubigeo = GeoPoint(ubi.getDouble("lat"), ubi.getDouble("lng"))
                    place.Ubicacion = ubigeo

                    val nom = placeOjectJs.getString("name")
                    place.nombre = nom
                    place.calificacion = random.nextDouble()*5
                    place.espacio = (0..100).random()
                    db.agregarHospital(place)
                    /**if (op == null) {
                        place.abierto = false
                    } else {
                        place.abierto = op.getBoolean("open_now")
                    }**/



                    /**val rating = placeOjectJs.getDouble("rating")
                    if (rating == null) {
                        place.calificacion = 0.0
                    } else {
                        place.calificacion = rating
                    }**/

                    mMap.addMarker(
                        MarkerOptions().position(
                            LatLng(
                               ubigeo.latitude,ubigeo.longitude
                            )
                        ).title(nom))

                }

            } catch (e: Exception) {
                Log.e("ERROR","error",e)
            }
        }

    }

