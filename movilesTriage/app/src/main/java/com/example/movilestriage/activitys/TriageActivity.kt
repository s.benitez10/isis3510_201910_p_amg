package com.example.movilestriage.activitys

import android.content.Context
import android.content.Intent
import android.icu.util.ULocale
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.example.movilestriage.R
import com.example.movilestriage.common.common
import com.example.movilestriage.database.sqliteManager
import com.example.movilestriage.model.Preguntas
import com.example.movilestriage.model.Tipos
import com.google.android.gms.common.internal.service.Common
import com.google.android.libraries.places.internal.v
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_preguntas.*
import kotlinx.android.synthetic.main.activity_triage.*
import kotlinx.android.synthetic.main.app_bar_preguntas.*
import kotlinx.android.synthetic.main.card_view_item.view.*
import kotlinx.android.synthetic.main.cita_view_item.view.*
import kotlinx.android.synthetic.main.content_preguntas.*
import kotlinx.android.synthetic.main.content_preguntas.view.*
import kotlinx.android.synthetic.main.fragment_pregunta.*
import java.lang.StringBuilder

class TriageActivity:  AppCompatActivity()  {

    private lateinit var  dbsqLite : sqliteManager
    private lateinit var  tiposList : MutableList<Tipos>
    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_triage)
        title= "Categorias"
        dbsqLite = sqliteManager(this)
        tiposList = dbsqLite.getTipos()
        linearLayoutManager = LinearLayoutManager(this)
        recycleListTipo.layoutManager =linearLayoutManager!!
        recycleListTipo.adapter = ListAdapterRelative( tiposList)
    }


}

private class ListAdapterRelative (val items : MutableList<Tipos>) : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{
    val tipos : MutableList<Tipos> =items
    class myViewHolderCard ( v: View) : RecyclerView.ViewHolder(v), View.OnClickListener
    {
        var view  :View = v
        var text : TextView? = v.card_Element
        var imagen: ImageView? =v.imageCard
        var id : Int?  = 0
        init {
            v.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            common.categoriaSelecionada = id
            common.categoriaEvaluada = v!!.card_Element.text.toString()
            val intent = Intent(v!!.context,PreguntasActivity::class.java)
            view.context.startActivity(intent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent!!.context)
        var card = inflater.inflate(R.layout.card_view_item, parent, false)


        return myViewHolderCard(card)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
       var card = holder as myViewHolderCard
        holder.text!!.text = items.get(position).tipo
        holder.id = items.get(position).id
        if(items.get(position).tipo == "Corazon")
        {
            holder.imagen!!.setImageResource(R.mipmap.icon_cora)
        }else  if(items.get(position).tipo == "Cabeza")
        {
            holder.imagen!!.setImageResource(R.mipmap.icon_cabe)
        }else  if(items.get(position).tipo == "Estremidades")
        {
            holder.imagen!!.setImageResource(R.mipmap.icon_huesos)
        }else  if(items.get(position).tipo == "Estomago")
        {
            holder.imagen!!.setImageResource(R.mipmap.icon_esto)
        }else
        {
            holder.imagen!!.setImageResource(R.mipmap.icon_pulmo)
        }

    }



}



// Preguntas Activity
// Revisar error del adapter

class PreguntasActivity : AppCompatActivity() {

    private lateinit var  dbsqLite : sqliteManager
    private lateinit var  listPreguntas : MutableList<Preguntas>
    private lateinit var linearLayoutManagerPreguntas: LinearLayoutManager
    private lateinit var  adapterP : ListAdapterPreguntas



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preguntas)
        setSupportActionBar(toolbar)
        dbsqLite=sqliteManager(this)
        preguntas()
        if(listPreguntas.size >0 && common.frametoPreguntas.size == 0)
        {
            for (i in listPreguntas.indices)
            {
                val bundle = Bundle()
                bundle.putInt("index",i)
                val fragment = preguntaFragment()
                fragment.arguments=bundle
                common.frametoPreguntas.add(fragment)

            }

            val fragAdap = ListAdapterPreguntas(supportFragmentManager,this,common.frametoPreguntas)
            viewpager.offscreenPageLimit = common.frametoPreguntas.size
            viewpager.adapter = fragAdap

            tabsPreguntas.setupWithViewPager(viewpager)
        }else if(listPreguntas.size >0 && common.frametoPreguntas.size > 0){

            val fragAdap = ListAdapterPreguntas(supportFragmentManager,this,common.frametoPreguntas)
            viewpager.offscreenPageLimit = common.frametoPreguntas.size
            viewpager.adapter = fragAdap

            tabsPreguntas.setupWithViewPager(viewpager)

        }



        viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            val izquierda = 0
            val derecha =1
            val indeterminado =2
            var actual = indeterminado

            private val direcionInderterminada : Boolean
                get() = actual == indeterminado
            private val direccionDerecha : Boolean
                get() = actual == derecha
            private val direccionIzquierda : Boolean
                get() = actual == izquierda

            private fun actualizarScrol( posicion :Float )
            {
                if(1-posicion >= 0.5)
                {
                    actual = derecha
                }
                else if(1-posicion <= 0.5)
                {
                    actual =izquierda
                }
            }

            override fun onPageScrollStateChanged(state: Int) {
                if(state == ViewPager.SCROLL_STATE_IDLE)
                {
                    actual = indeterminado
                }
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                if(direcionInderterminada)
                {
                    actualizarScrol(positionOffset)
                }
            }

            override fun onPageSelected(position: Int) {

                var preguntaActual : Preguntas
                var posicion =0

                if(posicion >0)
                {
                    if(direccionDerecha)
                    {
                        preguntaActual = listPreguntas[position-1]
                        posicion=position-1
                    }
                    else if (direccionIzquierda)
                    {
                        preguntaActual = listPreguntas[position+1]
                        posicion = position +1
                    }
                    else
                    {
                        preguntaActual =listPreguntas[posicion]
                    }
                }
                else
                {
                    preguntaActual =listPreguntas[0]
                    posicion =0
                }

            }

        })

    }


    private fun preguntas()
    {
        listPreguntas=dbsqLite.getPreguntas(common.categoriaSelecionada!!)
        common.preguntasCategoria = listPreguntas


        if (listPreguntas.size == 0)
        {
          AlertDialog.Builder(this)
              .setTitle("En Proceso")
              .setIcon(R.drawable.ic_whatshot_black_24dp)
              .setMessage("Esta seccion no esta completada")
              .show()
        }

    }
}

private class ListAdapterPreguntas (fm:FragmentManager,var context:Context,var fragmentList: List<Fragment>) : FragmentPagerAdapter(fm)
{
    override fun getItem(position: Int): Fragment {
       return fragmentList[position]
    }

    override fun getCount(): Int {
       return  fragmentList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {

            return StringBuilder("Pregunta ").append(position + 1).toString()

    }



}
