package com.example.movilestriage.model

import java.util.*

class Usuarios (

    val nombre: String?,
    val apellido: String?,
    val correo: String?,
    val edad: Int?,
    val triage_estado: String?

    ){
        constructor() : this(null,null,null,null,null )

        fun hashMapUsuario() : Map<String,Any?>
        {
            return mapOf(
                "apellido" to  apellido,
                "nombre" to nombre,
                "correo" to correo,
                "edad" to edad,
                "triage_estado" to triage_estado

            )
        }
}