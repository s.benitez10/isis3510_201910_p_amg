package com.example.movilestriage.common

import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import androidx.core.content.ContextCompat.getSystemService
import java.lang.Exception

class miradorInternet {


    lateinit var connectiManag :ConnectivityManager
    var contexto :Context
    var conectado : Boolean =false

    constructor(context: Context) {
        contexto=context.applicationContext
    }

    fun checkeo(): Boolean
    {
        try {
            connectiManag = contexto.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val informacion = connectiManag.activeNetworkInfo
            if(informacion!= null && informacion.isConnected )
            {
                conectado = true
            }else{
                conectado =false
            }

        }catch (e:Exception)
        {
            Log.v("connectivity", e.toString())
        }
        return  conectado
    }
}