package com.example.movilestriage.model

class Triage(
    val usuario :String?,
    val edad : Int?,
    val categoria : String?,
    val niveltriage:String?) {
    constructor(): this(null,null,null,null)
    fun hashMapTriage() : Map<String,Any?>
    {
        return mapOf(
            "usuario" to  usuario,
            "edad" to edad,
            "categoria" to categoria,
            "niveltriage" to niveltriage

        )
    }

}