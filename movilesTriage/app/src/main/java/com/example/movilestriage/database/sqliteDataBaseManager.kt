package com.example.movilestriage.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.movilestriage.model.Preguntas
import com.example.movilestriage.model.Tipos
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper

class sqliteManager(context: Context):SQLiteAssetHelper(context,"dataBaseMoviles.db",null,1){

    fun getTipos (): MutableList<Tipos>
    {
        val db : SQLiteDatabase = this.writableDatabase
        val objects = db.rawQuery("SELECT * FROM TIPOS_DOLOR", null)
        val tiposList = mutableListOf<Tipos>()

        if(objects.moveToFirst())
        {

            while (!objects.isAfterLast)
            {
                val tipoObj  = Tipos()
                tipoObj.id = objects.getInt(objects.getColumnIndex("ID"))
                tipoObj.tipo = objects.getString(objects.getColumnIndex("TIPOS"))
                tiposList.add(tipoObj)
                objects.moveToNext()
            }
        }
        objects.close()
        db.close()
        return tiposList
    }

    fun getPreguntas ( categoriaID : Int): MutableList<Preguntas>
    {
        val db : SQLiteDatabase = this.writableDatabase
        val objects = db.rawQuery("SELECT * FROM PREGUNTAS_CATEGORIA WHERE CATEGORIA_ID = ${categoriaID}", null)
        val PreguntasList = mutableListOf<Preguntas>()

        if(objects.moveToFirst())
        {

            while (!objects.isAfterLast)
            {
                val preguntObj  = Preguntas()
                preguntObj.id = objects.getInt(objects.getColumnIndex("ID"))
                preguntObj.pregunta = objects.getString(objects.getColumnIndex("PREGUNTA"))
                preguntObj.respuestaA = objects.getString(objects.getColumnIndex("RESPUESTA_A"))
                preguntObj.respuestaB = objects.getString(objects.getColumnIndex("RESPUESTA_B"))
                preguntObj.respuestaC = objects.getString(objects.getColumnIndex("RESPUESTA_C"))
                preguntObj.respuestaD = objects.getString(objects.getColumnIndex("RESPUESTA_D"))
                preguntObj.valorA = objects.getInt(objects.getColumnIndex("VALOR_RESPUESTA_A"))
                preguntObj.valorB = objects.getInt(objects.getColumnIndex("VALOR_RESPUESTA_B"))
                preguntObj.valorC = objects.getInt(objects.getColumnIndex("VALOR_RESPUESTA_C"))
                preguntObj.valorD = objects.getInt(objects.getColumnIndex("VALOR_RESPUESTA_D"))
                preguntObj.categoriaID = objects.getInt(objects.getColumnIndex("CATEGORIA_ID"))
                PreguntasList.add(preguntObj)
                objects.moveToNext()
            }
        }
        objects.close()
        db.close()
        return PreguntasList
    }
}