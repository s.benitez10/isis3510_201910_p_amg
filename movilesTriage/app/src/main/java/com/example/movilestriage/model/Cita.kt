package com.example.movilestriage.model

import com.google.firebase.firestore.GeoPoint
import java.util.*
import kotlin.collections.HashMap

data class Cita (

    val ciudad: String?,
    val doctor: String?,
    val especialidad: String?,
    val fecha: Date?,
    val usuario: String?,
    val ubicacion: GeoPoint?,
    val correo: String?,
    val hospital: String?
){
    constructor() : this(null,null,null,null,null,null,null,null)

    fun hashMapCita() : Map<String,Any?>
    {
        return mapOf(
            "ciudad" to  ciudad,
            "doctor" to doctor,
            "especialidad" to especialidad,
            "fecha" to fecha,
            "usuario" to usuario,
            "ubicacion" to ubicacion,
            "correo" to correo,
            "hospital" to hospital
        )
    }
}