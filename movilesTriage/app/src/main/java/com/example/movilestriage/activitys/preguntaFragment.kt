package com.example.movilestriage.activitys


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.Switch
import android.widget.TextView
import com.example.movilestriage.R
import com.example.movilestriage.common.common
import com.example.movilestriage.model.Preguntas
import kotlinx.android.synthetic.main.fragment_pregunta.*
import kotlinx.android.synthetic.main.fragment_pregunta.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class preguntaFragment : Fragment() {



    var pregunta : Preguntas? = null
    var indice =-1


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val obj= inflater.inflate(R.layout.fragment_pregunta, container, false)
        activity!!.title = "Prueba Triage"
        indice = arguments!!.getInt("index",-1)
        pregunta = common.preguntasCategoria[indice]
        if(pregunta != null)
        {

            obj.pregunta_triage.text = pregunta!!.pregunta

            obj.respuestaA_triage.text= pregunta!!.respuestaA
            obj.respuestaB_triage.text= pregunta!!.respuestaB

            if(pregunta!!.respuestaC  != null) {
                obj.respuestaC_triage.text = pregunta!!.respuestaC
            }
            else{
                obj.respuestaC_triage.visibility = View.GONE
                obj.RespuestaC.visibility=View.GONE
            }
            if(pregunta!!.respuestaD  != null) {
                obj.respuestaD_triage.text = pregunta!!.respuestaD

            }
            else{
                obj.respuestaD_triage.visibility = View.GONE
                obj.RespuestaD.visibility=View.GONE
            }

            obj.RespuestaA.setOnCheckedChangeListener{ CompoundButton, isCheked ->
                if(isCheked){
                    common.puntuacionTriage = common.puntuacionTriage + pregunta!!.valorA!!
                    obj.RespuestaB.isChecked = false
                    obj.RespuestaC.isChecked = false
                    obj.RespuestaD.isChecked = false
                }else
                {
                    common.puntuacionTriage = common.puntuacionTriage - pregunta!!.valorA!!
                }

            }
            obj.RespuestaB.setOnCheckedChangeListener{ CompoundButton, isCheked ->
                if(isCheked){
                    common.puntuacionTriage = common.puntuacionTriage + pregunta!!.valorB!!
                    obj.RespuestaA.isChecked = false
                    obj.RespuestaC.isChecked = false
                    obj.RespuestaD.isChecked = false
                }else
                {
                    common.puntuacionTriage = common.puntuacionTriage - pregunta!!.valorB!!
                }

            }
            obj.RespuestaC.setOnCheckedChangeListener{ CompoundButton, isCheked ->
                if(isCheked){
                    common.puntuacionTriage = common.puntuacionTriage + pregunta!!.valorC!!
                    obj.RespuestaB.isChecked = false
                    obj.RespuestaA.isChecked = false
                    obj.RespuestaD.isChecked = false
                }else
                {
                    common.puntuacionTriage = common.puntuacionTriage - pregunta!!.valorC!!
                }

            }
            obj.RespuestaD.setOnCheckedChangeListener{ CompoundButton, isCheked ->
                if(isCheked){
                    common.puntuacionTriage = common.puntuacionTriage + pregunta!!.valorD!!
                    obj.RespuestaB.isChecked = false
                    obj.RespuestaC.isChecked = false
                    obj.RespuestaA.isChecked = false
                }else
                {
                    common.puntuacionTriage = common.puntuacionTriage - pregunta!!.valorD!!
                }

            }
            if(indice ==4)
            {
                obj.resutado_btn.visibility = View.VISIBLE
                obj.resutado_btn.setOnClickListener {
                    val intent = Intent(this.context,
                        Resultado_Activity::class.java)
                    startActivity(intent)

                }
            }

        }

        return obj
    }

}
