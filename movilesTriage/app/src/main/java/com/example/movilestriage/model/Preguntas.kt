package com.example.movilestriage.model

data class Preguntas(
    var id : Int?,
    var pregunta: String?,
    var respuestaA: String?,
    var respuestaB: String?,
    var respuestaC: String?,
    var respuestaD: String?,
    var valorA: Int?,
    var valorB: Int?,
    var valorC: Int?,
    var valorD: Int?,
    var categoriaID: Int?,
    var estado:Boolean
) {
    constructor() : this(null,null,null,null,null,null,null,null,null,null,null,false)


}