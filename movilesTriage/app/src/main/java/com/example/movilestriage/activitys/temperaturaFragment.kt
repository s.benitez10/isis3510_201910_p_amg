package com.example.movilestriage.activitys



import android.content.Context
import android.content.pm.ServiceInfo
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast


import com.example.movilestriage.R
import kotlinx.android.synthetic.main.fragment_temperatura.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class temperaturaFragment : Fragment(),SensorEventListener {

    private lateinit var sensorManager : SensorManager
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        this.sensorManager = activity!!.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        return inflater.inflate(R.layout.fragment_temperatura, container, false)
    }

    override fun onResume() {
        super.onResume()
        temperatura()
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSensorChanged(event: SensorEvent?) {
        if (event!!.values.size > 0) {
            val temperaturaEvento = event.values[0];
            temperatura_txt.text =temperaturaEvento.toString();

        }
    }

    fun temperatura()
    {
        val sensorTemp = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE)
        if (sensorTemp != null) {
            sensorManager.registerListener(this, sensorTemp, SensorManager.SENSOR_DELAY_FASTEST)
        } else {
            Toast.makeText(this.context, "No tiene el sensor ", Toast.LENGTH_LONG).show()

        }
    }

}
