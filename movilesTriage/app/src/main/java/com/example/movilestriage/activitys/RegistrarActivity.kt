package com.example.movilestriage.activitys

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.ListView
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import com.example.movilestriage.R
import com.example.movilestriage.common.common
import com.example.movilestriage.database.FirebaseManager
import com.example.movilestriage.model.Usuarios
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_autenticacion_firebase.*

import kotlinx.android.synthetic.main.activity_registrar.*
import kotlinx.android.synthetic.main.content_registrar.*
import java.text.SimpleDateFormat
import java.time.LocalDate

class RegistrarActivity : AppCompatActivity() {

    companion object {

        private const val RC_SIGN_IN2 = 123
        const val RC_SIGN_IN = 123
        private const val TAG = "Login"
    }
    private  lateinit var intentMenu : Intent
    private lateinit var auth: FirebaseAuth
    private lateinit var  db: FirebaseManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_registrar)
        registrar_button.setOnClickListener {view ->
            if(validateForm(view))
            {
            agregarUsuario()
            }
            createAccount(Email_text.text.toString(), contraseña_text.text.toString(),view)
        }
        intentMenu = Intent(this,MenuActivity::class.java)
        auth = FirebaseAuth.getInstance()
        db= FirebaseManager()
        title ="Registro"
    }

    private fun createAccount(email: String, password: String, view: View) {
        Log.d("Login", "createAccount:$email")

        if (!validateForm()) {
            return
        }



        // [START create_user_with_email]
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("Login", "createUserWithEmail:success")
                    common.usuarioActual = Email_text.text.toString()
                    startActivity(intentMenu)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("Login", "createUserWithEmail:failure", task.exception)
                    Snackbar.make(view,"Autenticacion fallida",Snackbar.LENGTH_SHORT).show()
                }


            }

        // [END create_user_with_email]
    }

    private fun validateForm(view:View): Boolean {
        var valid = true

        val email = Email_text.text.toString()
        if (TextUtils.isEmpty(email)) {
            Email_text.error = "Requerido."
            valid = false
        } else {
            Email_text.error = null
        }

        val password = contraseña_text.text.toString()
        if (TextUtils.isEmpty(password)) {
            contraseña_text.error = "Requerido."
            valid = false
        } else {
            contraseña_text.error = null
        }

        val nombre = nombre_text.text.toString()
        if (TextUtils.isEmpty(nombre)) {
            nombre_text.error = "Requerido."
            valid = false
        } else {
            nombre_text.error = null
        }

        val apellido = apellido_text.text.toString()
        if (TextUtils.isEmpty(apellido)) {
            apellido_text.error = "Requerido."
            valid = false
        } else {
            apellido_text.error = null
        }

        val Validacion = confirmation_contraseña.text.toString()
        if (TextUtils.isEmpty(Validacion)) {
            confirmation_contraseña.error = "Requerido."
            valid = false
        } else {
            confirmation_contraseña.error = null
        }

        if(Validacion != password)
        {
            Snackbar.make(view,"Las contraseñas no coinciden",Snackbar.LENGTH_SHORT).show()
            valid=false
        }


        return valid
    }

    private fun agregarUsuario()
    {

        val fecha = SimpleDateFormat("YYYYY-mm-dd").parse(LocalDate.now().toString())
        val usuario = Usuarios(nombre_text.text.toString(),apellido_text.text.toString(),Email_text.text.toString(),edad_text.toString().toInt(),"No evaluado")
        db.AgregarUsuario(usuario)
    }
}
