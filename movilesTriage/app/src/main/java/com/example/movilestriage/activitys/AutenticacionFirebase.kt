package com.example.movilestriage.activitys

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.movilestriage.R
import com.example.movilestriage.common.common
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_autenticacion_firebase.*
import kotlinx.android.synthetic.main.app_bar_preguntas.*

class AutenticacionFirebase : AppCompatActivity() {

    companion object {

        private const val RC_SIGN_IN2 = 123
        const val RC_SIGN_IN = 123
        private const val TAG = "Login"
    }
    private  lateinit var intentMenu :Intent
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_autenticacion_firebase)
        sing_in_button.setOnClickListener{view ->
            signIn(email_text.text.toString(), password_text.text.toString(),view)
        }
        register_Button.setOnClickListener{
            val registrar = Intent(this,RegistrarActivity::class.java)
            startActivity(registrar)

        }
        title="Ingresar"
        intentMenu = Intent(this,MenuActivity::class.java)
        auth = FirebaseAuth.getInstance()
    }

    override fun onStart() {
        super.onStart()
        val usuarioActual = auth.currentUser

    }



    private fun validateForm(): Boolean {
        var valid = true

        val email = email_text.text.toString()
        if (TextUtils.isEmpty(email)) {
            email_text.error = "Requerido."
            valid = false
        } else {
            email_text.error = null
        }

        val password = password_text.text.toString()
        if (TextUtils.isEmpty(password)) {
            password_text.error = "Requerido."
            valid = false
        } else {
            password_text.error = null
        }

        return valid
    }



    private fun signIn(email: String, password: String, view:View) {
        Log.d(TAG, "signIn:$email")
        if (!validateForm()) {
            return
        }


        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "Ingreso autorizado")
                    common.usuarioActual = email_text.text.toString()
                    Log.d("usuario", "" + auth.currentUser)
                    startActivity(intentMenu)

                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "El email no conside", task.exception)
                    Snackbar.make(view,"Autenticacion fallida",Snackbar.LENGTH_SHORT).show()
                }


            }

    }
}

