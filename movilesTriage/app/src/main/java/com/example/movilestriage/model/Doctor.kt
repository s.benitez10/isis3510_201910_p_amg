package com.example.movilestriage.model

import com.google.firebase.firestore.GeoPoint
import java.util.*

data class Doctor (

    val ciudad: String?,
    val nombre: String?,
    val especialidad: String?,
    val calificacion: Double?,
    val hospital: String?

){
    constructor() : this(null,null,null,null,null )

    fun hashMapDoctor() : Map<String,Any?>
    {
        return mapOf(
            "ciudad" to  ciudad,
            "nombre" to nombre,
            "especialidad" to especialidad,
            "calificacion" to calificacion,
            "hospital" to hospital

        )
    }

}