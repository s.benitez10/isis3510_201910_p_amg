package com.example.movilestriage.activitys

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.movilestriage.R
import com.example.movilestriage.common.common
import com.example.movilestriage.common.miradorInternet
import com.example.movilestriage.database.FirebaseManager
import com.example.movilestriage.model.Cita
import com.example.movilestriage.model.Doctor
import com.example.movilestriage.model.Usuarios
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.GeoPoint
import com.google.type.Date
import kotlinx.android.synthetic.main.activity_agenda.*
import kotlinx.android.synthetic.main.activity_formcita.*

import kotlinx.android.synthetic.main.cita_view_item.view.*
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Year
import java.util.*

val db = FirebaseManager()

class AgendaActivity: AppCompatActivity() {

    lateinit var myrunnableCitas :Runnable
    lateinit var  myhanddlerCitas :Handler
    private lateinit var  connector : miradorInternet

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        myhanddlerCitas = Handler()

        connector = miradorInternet(this)
        if(connector.checkeo()) {
            setContentView(R.layout.activity_agenda)
        }
        else
        {
            setContentView(R.layout.no_internet_layout)
        }
        title ="Citas"

        if(connector.checkeo()) {
            db.LisenerCita()
            myrunnableCitas = Runnable {

                db.consultarCitas()
                if (agenda_citas != null) {
                    agenda_citas.adapter = adapterCita()
                }
            }
            add_cita.setOnClickListener {
                val intent = Intent(this, AddAgendaActivity::class.java)
                startActivity(intent)
            }
            myhanddlerCitas.postDelayed(myrunnableCitas, 1000)


        }

    }


    override fun onDestroy() {
        super.onDestroy()
        db.cerrarListnerCitas()
    }


}

private class adapterCita (): BaseAdapter() {

    private var citas : MutableList<Cita>
    init {
        citas = db.getCitas()
    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        citas = db.getCitas()
        val cell :View
        if(convertView == null)
        {
            val inflater = LayoutInflater.from(parent!!.context)
            cell = inflater.inflate(R.layout.cita_view_item,parent,false)
            val viewHolder =ViewHolder(cell.textView2,cell.textView3)
            Log.v("textView", "Llamado findbyid")
            cell.tag = viewHolder
        }else
        {
            cell =convertView
        }
        val viewHolder = cell.tag as ViewHolder
        viewHolder.nombreCiudad.text = citas.get(position).ciudad
        viewHolder.nombreDoctor.text = citas.get(position).doctor
        return cell
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): Any {
        return "String" //To change body of created functions use File | Settings | File Templates.
    }

    override fun getCount(): Int {
        return citas.size//To change body of created functions use File | Settings | File Templates.
    }

    private class ViewHolder(val nombreCiudad : TextView ,val nombreDoctor: TextView)
    {

    }
}

// SECOND VIEW OF THE FEATURE ADD AGENDA
var ciudadtext=""
var tipotext =""
var especialidadText=""
var nombreDoctor =""
var ciudadDoctor =""
var especialidadDoctor=""
var fechaCita =""
var nombrePaciente =""
var hospitalDoctor =""
var ubicacion = GeoPoint(0.0,0.0)

lateinit var myrunnableDoctor :Runnable
lateinit var  myhanddlerDoctor :Handler

lateinit var myrunnableUsuario :Runnable
lateinit var  myhanddlerUsuario :Handler

class AddAgendaActivity: AppCompatActivity() {
    private lateinit var disDate: EditText
    private lateinit var disDateListener: DatePickerDialog.OnDateSetListener
    private lateinit var  connector : miradorInternet

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        myhanddlerDoctor = Handler()
        usuariosCita().execute(common.usuarioActual)

        connector= miradorInternet(this)
        if(connector.checkeo()) {
            setContentView(R.layout.activity_formcita)
        }
        else{
            setContentView(R.layout.no_internet_layout)

        }
        title ="Agendar Cita"

        if(connector.checkeo()) {
            editFecha.setOnClickListener(View.OnClickListener { view ->
                val cal = Calendar.getInstance()
                val ano = cal.get(Calendar.YEAR)
                val mes = cal.get(Calendar.MONTH)
                val dia = cal.get(Calendar.DAY_OF_MONTH)

                val dialogo = DatePickerDialog(
                    this,
                    R.style.Base_Theme_AppCompat_Dialog_MinWidth,
                    disDateListener,
                    ano,
                    mes,
                    dia
                )
                dialogo.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialogo.show()
            })

            disDateListener =
                DatePickerDialog.OnDateSetListener() { view: DatePicker?, year: Int, month: Int, dayOfMonth: Int ->
                    editFecha.text = "${month}/${dayOfMonth}/${year}"
                    fechaCita = editFecha.text.toString()
                }
            addDate.setOnClickListener { view ->

                agregarCita(view)

            }

            ArrayAdapter.createFromResource(this, R.array.especialidades_array, android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                especialidad.adapter = adapter
                especialidad.onItemSelectedListener = SpinnerActivity1()
            }
            ArrayAdapter.createFromResource(this, R.array.Tipo_array, android.R.layout.simple_spinner_item)
                .also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    tipo.adapter = adapter
                    tipo.onItemSelectedListener = SpinnerActivity2()
                }
            ArrayAdapter.createFromResource(this,R.array.Ciudad_array, android.R.layout.simple_spinner_item)
                .also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    editText.adapter = adapter
                    editText.onItemSelectedListener = SpinnerActivity3()
                }
            db.LisenerDoctor()
            myrunnableDoctor = Runnable { doctor_List.adapter = adapterDoctor() }
            doctor_List.setOnItemClickListener { parent, view, position, id ->

                val celda: Doctor = doctor_List.getItemAtPosition(position) as Doctor

                nombreDoctor = celda.nombre.toString()
                especialidadDoctor = celda.especialidad.toString()
                ciudadDoctor = celda.ciudad.toString()
                hospitalDoctor = celda.hospital.toString()
                Snackbar.make(view,"Doctor seleccionado",Snackbar.LENGTH_SHORT ).show()
            }
        }
    }



    override fun onDestroy() {
        super.onDestroy()

        db.cerrarListnerDoctor()
    }
    fun agregarCita(view: View) {


        Log.d("ERROR:","${tipotext} ${especialidadText} ${nombreDoctor} ${nombrePaciente} ${fechaCita} ")
        if (tipotext == "" || tipotext == "Tipo"
            || especialidadText == "" || especialidadText == "Especialidad"
            || nombreDoctor == "" || fechaCita == ""
            || ciudadDoctor =="Ciudad" || ciudadDoctor =="") {

           var snack = Snackbar.make(view,"Llenar toda las condiciones",Snackbar.LENGTH_SHORT )
            snack.setActionTextColor(Color.RED)
            snack.show()

        } else {
                val fechaCi = SimpleDateFormat("mm/dd/yyyy").parse(fechaCita)
                val usuario = db.getUsuario()
                val cita = Cita(ciudadDoctor, nombreDoctor, especialidadText, fechaCi, usuario.nombre + " " + usuario.apellido, ubicacion, usuario.correo,
                    hospitalDoctor)
                db.agregarCita(cita)
                val intent = Intent(this, MenuActivity::class.java)
                startActivity(intent)
            }

        }

    }


private class SpinnerActivity1 : Activity(), AdapterView.OnItemSelectedListener {

    override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
        // An item was selected. You can retrieve the selected item using
         especialidadText = parent.getItemAtPosition(pos).toString()
        if(tipotext != "" && tipotext != "Tipo"  &&
            especialidadText != "" && especialidadText != "Especialista"
            && ciudadtext != "" && ciudadtext != "Ciudad" )
        {

            db.consultarDoctoresEspecificos(especialidadText, ciudadtext)
            myhanddlerDoctor.post(myrunnableDoctor)

        }

    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // Another interface callback
    }
}

private class SpinnerActivity2 : Activity(), AdapterView.OnItemSelectedListener {

    override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
        // An item was selected. You can retrieve the selected item using
        tipotext = parent.getItemAtPosition(pos).toString()
        if(tipotext != "" && tipotext != "Tipo"  &&
            especialidadText != "" && especialidadText != "Especialista"
            && ciudadtext != "" && ciudadtext != "Ciudad" )
        {

            db.consultarDoctoresEspecificos(especialidadText, ciudadtext)
            myhanddlerDoctor.post(myrunnableDoctor)
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // Another interface callback
    }
}


private class SpinnerActivity3 : Activity(), AdapterView.OnItemSelectedListener {

    override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
        // An item was selected. You can retrieve the selected item using
        ciudadtext = parent.getItemAtPosition(pos).toString()
        if(tipotext != "" && tipotext != "Tipo"  &&
            especialidadText != "" && especialidadText != "Especialista"
            && ciudadtext != "" && ciudadtext != "Ciudad" )
        {

            db.consultarDoctoresEspecificos(especialidadText, ciudadtext)
            myhanddlerDoctor.post(myrunnableDoctor)
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // Another interface callback
    }
}


private class adapterDoctor (): BaseAdapter() {

    private var doctores: MutableList<Doctor>

    init {
        doctores = db.getDoctores()
        if (especialidadText != "" && especialidadText != "Especialista") {
            filtrarDoctores(especialidadText)
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

            val inflater = LayoutInflater.from(parent!!.context)
            val cell = inflater.inflate(R.layout.cita_view_item, parent, false)
            cell.textView2.text = doctores.get(position).especialidad
            cell.textView3.text = doctores.get(position).nombre
            return cell


    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): Any {
        return doctores.get(position) //To change body of created functions use File | Settings | File Templates.
    }

    override fun getCount(): Int {
        return doctores.size//To change body of created functions use File | Settings | File Templates.
    }

    private fun filtrarDoctores(especialidad : String)
    {
        var doctoresFil : MutableList<Doctor> = mutableListOf()
        for(doc in doctores)
        {
            if(doc.especialidad.equals(especialidad))
            {
                doctoresFil.add(doc)
            }
        }

        doctores =doctoresFil

    }
}

private class usuariosCita : AsyncTask<String, Void, Usuarios>() {

    override fun doInBackground(vararg params: String?):Usuarios{

        db.obtenerUsuario()
        return db.getUsuario()
    }

}