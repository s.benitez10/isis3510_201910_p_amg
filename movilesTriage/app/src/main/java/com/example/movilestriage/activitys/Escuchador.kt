package com.example.movilestriage.activitys

import android.util.Log
import com.google.android.gms.wearable.DataEventBuffer
import com.google.android.gms.wearable.DataMapItem
import com.google.android.gms.wearable.WearableListenerService

class Escuchador : WearableListenerService() {

    override fun onDataChanged(dataEvents: DataEventBuffer) {
        dataEvents.forEach { event ->


            event.dataItem.also { item ->
                if (item.uri.path.compareTo("/pulso/") == 0) {
                    DataMapItem.fromDataItem(item).dataMap.apply {
                        val valor= getInt("MY_KEY")
                        Log.d("Valor","valor " + valor)
                    }
                }
            }
        }
    }
}