package com.example.movilestriage.database

import android.content.ContentValues.TAG
import android.provider.Settings
import android.util.Log
import com.example.movilestriage.common.common
import com.example.movilestriage.model.*
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.*
import com.google.firebase.firestore.EventListener
import java.util.*


class FirebaseManager {

    private var citas : MutableList<Cita> = mutableListOf()
    private var doctore : MutableList<Doctor> = mutableListOf()
    val db = FirebaseFirestore.getInstance()
    private lateinit var listenerCita: ListenerRegistration
    private lateinit var listenerDoctor: ListenerRegistration
    private  var usuarioActual:  Usuarios = Usuarios()
    init {

    }

    fun agregarCita( data: Cita)
    {
        val hashcita = data.hashMapCita()
        db.collection("Citas").add(hashcita).addOnSuccessListener {documentReference ->
            Log.d("Agrego:", "DocumentoSnapshot added with ID: ${documentReference.id}")
        }.addOnFailureListener { e ->
            Log.w("falla", "Error adding document", e)
        }

    }

    fun agregarDoctor( data: Doctor)
    {
        val hashcita = data.hashMapDoctor()
        db.collection("Doctores").add(hashcita).addOnSuccessListener {documentReference ->
            Log.d("Agrego:", "DocumentoSnapshot added with ID: ${documentReference.id}")
        }.addOnFailureListener { e ->
            Log.w("falla", "Error adding document", e)
        }

    }

    fun AgregarUsuario(data: Usuarios)
    {
        val hashUsuario = data.hashMapUsuario()
        db.collection("Usuarios").add(hashUsuario).addOnSuccessListener { documentReference ->
            Log.d("Agrego:", "DocumentoSnapshot added with ID: ${documentReference.id}") }
            .addOnFailureListener{ e ->
                Log.w("falla", "Error adding document", e)
            }
    }
    fun agregarHospital (data: Place)
    {
        val hashPlace = data.hashMapPlace()
        db.collection("Hospitales").add(hashPlace).addOnSuccessListener { documentReference ->
            Log.d("Agrego:", "DocumentoSnapshot added with ID: ${documentReference.id}") }
            .addOnFailureListener{ e ->
                Log.w("falla", "Error adding document", e)
            }
    }
    fun agregarTriage (data: Triage)
    {
        val hashTriage = data.hashMapTriage()
        db.collection("Triage").add(hashTriage).addOnSuccessListener { documentReference ->
            Log.d("Agrego:", "DocumentoSnapshot added with ID: ${documentReference.id}") }
            .addOnFailureListener{ e ->
                Log.w("falla", "Error adding document", e)
            }
    }
    fun obtenerUsuario()
    {

        db.collection("Usuarios").whereEqualTo("correo",common.usuarioActual).get()
            .addOnSuccessListener { result ->

                usuarioActual=result.toObjects(Usuarios::class.java)[0]
                Log.d("Usuario encontrado","este es el usuario "+ usuarioActual.nombre)
                common.nombreUsuarioActual = usuarioActual.nombre!!
                common.edadUsuarioActual =usuarioActual.edad.toString()
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }
    }
    fun ListenerUsuarios()
    {
        db.collection("Usuarios").whereEqualTo("correo",common.usuarioActual).addSnapshotListener(EventListener<QuerySnapshot> {snapshot,e ->
            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@EventListener
            }

            if (snapshot != null ) {

                val cit = snapshot.toObjects(Usuarios::class.java)
                usuarioActual= cit[0]
                Log.d(TAG, "Current data: " + snapshot)
            } else {
                Log.d(TAG, "Current data: null")
            }

        })

    }
    // do
    fun LisenerDoctor()
    {
         listenerDoctor=db.collection("Doctores").addSnapshotListener(EventListener<QuerySnapshot> {snapshot,e ->
            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@EventListener
            }

            if (snapshot != null ) {

                val cit = snapshot.toObjects(Doctor::class.java)
                doctore = cit
                Log.d(TAG, "Current data: " + snapshot)
            } else {
                Log.d(TAG, "Current data: null")
            }

        }
        )


    }
    fun cerrarListnerDoctor()
    {
        listenerDoctor.remove()
    }

    fun LisenerCita()
    {

        listenerCita= db.collection("Citas").whereEqualTo("correo",common.usuarioActual).addSnapshotListener(EventListener<QuerySnapshot> {snapshot,e ->
            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@EventListener
            }

            if (snapshot != null ) {
                val cit = snapshot.toObjects(Cita::class.java)
                citas = cit
                Log.d(TAG, "Current data: " + snapshot)
            } else {
                Log.d(TAG, "Current data: null")
            }

        }
        )


    }

    fun cerrarListnerCitas()
    {
        listenerCita.remove()
    }
    // fetch data

   fun consultarCitas()
   {
       db.collection("Citas").whereEqualTo("correo",common.usuarioActual)
           .get()
           .addOnSuccessListener { result ->

               val cit =result.toObjects(Cita::class.java)
               citas = cit


               Log.d("Agrego", "todas las citas consultadas}")
           }
           .addOnFailureListener { exception ->
               Log.w("Fallo", "Error getting documents.", exception)
           }
   }
    fun consultarDoctores()
    {
        db.collection("Doctores")
            .get()
            .addOnSuccessListener { result ->

                val cit =result.toObjects(Doctor::class.java)
                doctore = cit
                Log.d("Agrego", "todas las citas consultadas}")
            }
            .addOnFailureListener { exception ->
                Log.w("Fallo", "Error getting documents.", exception)
            }
    }

    fun consultarDoctoresEspecificos(pEspecialidad : String, pCiudad :String)
    {
        db.collection("Doctores").whereEqualTo("especialidad",pEspecialidad).whereEqualTo("ciudad",pCiudad).get()

            .addOnSuccessListener { result ->

            val cit =result.toObjects(Doctor::class.java)
            doctore = cit
            Log.d("Agrego", "todas las citas consultadas}")
            }
            .addOnFailureListener { exception ->
                Log.w("Fallo", "Error getting documents.", exception)
            }

    }

    // Getters of data
    fun getCitas() :MutableList<Cita>
    {
        return citas
    }

    fun getDoctores() :MutableList<Doctor>
    {
        return doctore
    }
    fun getUsuario() :Usuarios
    {
        return usuarioActual
    }

}