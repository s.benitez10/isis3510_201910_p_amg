package com.example.movilestriage.common

import androidx.fragment.app.Fragment
import com.example.movilestriage.activitys.preguntaFragment
import com.example.movilestriage.model.Preguntas
import com.example.movilestriage.model.Tipos

object common {

    var categoriaSelecionada : Int? = null
    var frametoPreguntas : MutableList<Fragment> = ArrayList()
    var preguntasCategoria : MutableList<Preguntas> = ArrayList()
    var puntuacionTriage : Int = 0
    var usuarioActual:String =""
    var edadUsuarioActual :String=""
    var nombreUsuarioActual: String=""
    var triageActual:String=""
    var categoriaEvaluada:String=""
}