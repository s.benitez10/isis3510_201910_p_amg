package com.example.movilestriage.activitys

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.movilestriage.R
import kotlinx.android.synthetic.main.activity_tramites_menu.*

class TramitesMenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tramites_menu)
        Card1.setOnClickListener {view->

            val intent = Intent(this,TramiteResultadosActivity::class.java)
            startActivity(intent)
        }
        Card2.setOnClickListener {view->

            Toast.makeText(this,"Card 2",Toast.LENGTH_SHORT).show();
        }
    }
}



