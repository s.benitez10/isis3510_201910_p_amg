package com.example.movilestriage.activitys

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.movilestriage.R
import com.example.movilestriage.database.FirebaseManager
import com.example.movilestriage.model.Cita
import com.example.movilestriage.model.Doctor
import com.firebase.ui.auth.AuthUI
import com.google.firebase.firestore.GeoPoint
import kotlinx.android.synthetic.main.activity_menu.*
import java.util.*
import kotlin.collections.ArrayList

class MenuActivity : AppCompatActivity() {

    val db = FirebaseManager()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        title = "Menu"

        triage.setOnClickListener{
            val intent = Intent(this,TriageActivity::class.java)
            startActivity(intent)
        }
        agenda.setOnClickListener{
            val intent = Intent(this,AgendaActivity::class.java)
            startActivity(intent)
        }
        hospital.setOnClickListener{
            val intent = Intent(this,MapsActivity::class.java)
            startActivity(intent)

        }

        tramites.setOnClickListener {
            //db.agregarCita(cita)
            val intent = Intent(this,TramitesMenuActivity::class.java)
            startActivity(intent)
        }

    }

}
