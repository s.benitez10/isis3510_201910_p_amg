package com.example.wearhb

import android.app.Service
import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.support.wearable.activity.WearableActivity
import android.util.Log
import android.widget.Toast
import com.google.android.gms.common.api.GoogleApi
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.Task
import com.google.android.gms.wearable.*
import kotlinx.android.synthetic.main.activity_main.*
import java.nio.ByteBuffer

class MainActivity : WearableActivity(),SensorEventListener{

    private lateinit var sensorManager : SensorManager
    private lateinit var dataClient: DataClient




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        setContentView(R.layout.activity_main)
        dataClient = Wearable.getDataClient(this)

        activarSensores()
        // Enables Always-on
        setAmbientEnabled()
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

    }

    override fun onSensorChanged(event: SensorEvent?) {



            if (event!!.values.size > 0) {
                val corazonEvento = event.values[0]
                val putDataReq: PutDataRequest = PutDataMapRequest.create("/pulso/").run {
                    dataMap.putInt("MY_KEY", corazonEvento.toInt())
                    asPutDataRequest()
                }
                Log.d("Ritmo", corazonEvento.toString())
                corazon.text = corazonEvento.toString();
                val putDataTask: Task<DataItem> = dataClient.putDataItem(putDataReq)
                putDataTask.addOnSuccessListener {

                        Log.d("HOLA","enviando datos " + it.data)
                }

            }

    }

    fun activarSensores()
    {
        var pulso = sensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE)


        if (pulso!= null) {
            sensorManager.registerListener(this, pulso, SensorManager.SENSOR_DELAY_NORMAL)
            Toast.makeText(this, "Si hay sensor ", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, "No tiene el sensor HEART_BEAT ", Toast.LENGTH_LONG).show()

        }


    }


}
